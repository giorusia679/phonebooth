public class Payment {

    private double amount;

    public Payment() {
        this.amount = 0.0;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
