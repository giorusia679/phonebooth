public class User {
    private Payment payment;

    public User() {
        this.payment = new Payment();
    }

    public void makePayment(double amount) {
        payment.setAmount(payment.getAmount() + amount);
    }

    public Payment getPayment() {
        return payment;
    }

    public boolean hasSufficientPayment() {
        return payment.getAmount() > 0;
    }
}
