public class PhoneBooth {
    private User user;
    private Call call;
    private Payment payment;

    public PhoneBooth() {
        this.user = new User();
        this.call = new Call();
        this.payment = new Payment();
    }

    // Functionality: Allow users to insert payment
    public void insertPayment(double amount) {
        user.makePayment(amount);
    }

    // Functionality: Allow users to make a call
    // Usability: Provide a user-friendly way to initiate calls
    public void makeCall(DialPad number) {
        // Check if there is sufficient payment before making a call
        if (user.hasSufficientPayment()) {
            call.startCall();
            // Assuming some logic to make the call to the provided number
            System.out.println("Calling " + number + "...");
            // ...

            // End the call after a certain duration based on payment
            call.endCall();
        } else {
            System.out.println("Insufficient payment. Please insert more money.");
        }
    }

    public User getUser() {
        return user;
    }

    public Call getCall() {
        return call;
    }

    public Payment getPayment() {
        return payment;
    }
}
