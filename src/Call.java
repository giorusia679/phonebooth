public class Call {
    private double duration;

    public Call() {
        this.duration = 0;
    }

    public void startCall() {
        // Assume call is started
    }

    public void endCall() {
        // Assume call is ended
    }

    public double getDuration() {
        return duration;
    }
}
